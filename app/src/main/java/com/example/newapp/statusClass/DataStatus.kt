package com.example.newapp.statusClass

import okhttp3.ResponseBody

sealed class DataStatus<out Ts> {
    data class Success<T>(val data: T) : DataStatus<T>()
    data class Failure(val message: ResponseBody) : DataStatus<Nothing>()
    data class TimeOut(val message: String) : DataStatus<Nothing>()
}