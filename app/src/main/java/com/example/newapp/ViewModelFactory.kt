package com.example.newapp

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.newapp.repository.DataRepository

class ViewModelFactory: ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        with(modelClass) {
             when {
                isAssignableFrom(MarvelDataViewModel::class.java) -> MarvelDataViewModel(
                    dataRepository = DataRepository()
                )
                 else -> throw IllegalArgumentException(
                     "Unknown ViewModel class: ${modelClass.name}")
             }

        }as T
}