package com.example.newapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.newapp.R
import com.example.newapp.adapter.viewHolder.MarvelItemHolder
import com.example.newapp.model.Response.Results

class MarvelAdapter : ListAdapter<Results, RecyclerView.ViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<Results>() {
        override fun areItemsTheSame(oldItem: Results, newItem: Results): Boolean =
            oldItem == newItem

        override fun areContentsTheSame(oldItem: Results, newItem: Results): Boolean =
            oldItem.id == newItem.id
    }
    var itemClick:((Results)-> Unit)? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MarvelItemHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_marval_index, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is MarvelItemHolder -> holder.setData(getItem(position)) {
                itemClick?.invoke(it)
            }
        }
    }
}
