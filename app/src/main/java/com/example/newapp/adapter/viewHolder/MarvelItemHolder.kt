package com.example.newapp.adapter.viewHolder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.newapp.R
import com.example.newapp.model.Response.Results
import kotlinx.android.synthetic.main.item_marval_index.view.*

class MarvelItemHolder(view: View): RecyclerView.ViewHolder(view) {

    fun setData(result: Results, itemClick:(Results)-> Unit) {
        itemView.apply {
            val img = result.thumbnail
            val url = if ("${img?.path}".contains("http")) {
                img?.path?.substring(5,img.path.length)
            }else img?.path
            val imgUrl = if ("$url".contains("image_not_available"))"https:${url}.${img?.extension}"
            else "https:${url}/portrait_uncanny.${img?.extension}"
            result.thumbnail?.imgUrl = imgUrl
            Glide.with(context)
                .load(result.thumbnail?.imgUrl)
                .fitCenter()
                .placeholder(R.drawable.ic_cloud_download_24)
                .into(ig_icon)
            name.text = result.title
            result.thumbnail?.extension
            setOnClickListener {
                itemClick.invoke(result)
            }
        }
    }
}