package com.example.newapp.model.Response


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import kotlinx.serialization.Serializable

@Keep
@Serializable
data class TextObject(
    @SerializedName("language")
    var language: String = "",
    @SerializedName("text")
    var text: String = "",
    @SerializedName("type")
    var type: String = ""
)