package com.example.newapp.model.Response


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import kotlinx.serialization.Serializable

@Keep
@Serializable
data class Url(
    @SerializedName("type")
    var type: String = "",
    @SerializedName("url")
    var url: String = ""
)