package com.example.newapp.model.Response


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import kotlinx.serialization.Serializable

@Keep
@Serializable
data class Characters(
    @SerializedName("available")
    var available: Int = 0,
    @SerializedName("collectionURI")
    var collectionURI: String = "",
    @SerializedName("items")
    var items: List<Item> = listOf(),
    @SerializedName("returned")
    var returned: Int = 0
)