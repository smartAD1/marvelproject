package com.example.newapp.model.Response


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Data(
    @SerializedName("count")
    var count: Int = 0,
    @SerializedName("limit")
    var limit: Int = 0,
    @SerializedName("offset")
    var offset: Int = 0,
    @SerializedName("results")
    var results: ArrayList<Results> = arrayListOf(),
    @SerializedName("total")
    var total: Int = 0
)