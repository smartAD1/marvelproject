package com.example.newapp.model.Response


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import kotlinx.serialization.Serializable

@Keep
@Serializable
data class Image(
    @SerializedName("extension")
    var extension: String = "",
    @SerializedName("path")
    var path: String = ""
)