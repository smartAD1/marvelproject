package com.example.newapp.model.Response


import com.google.gson.annotations.SerializedName
import kotlinx.serialization.Serializable


@Serializable
data class Results(
    @SerializedName("characters")
    var characters: Characters? = Characters(),
    @SerializedName("collectedIssues")
    var collectedIssues: List<CollectedIssue>? = listOf(),
//    @SerializedName("collections")
//    var collections: List<Any> = listOf(),
    @SerializedName("creators")
    var creators: Creators? = Creators(),
    @SerializedName("dates")
    var dates: List<Date>? = listOf(),
    @SerializedName("description")
    val description: String? = "",
    @SerializedName("diamondCode")
    var diamondCode: String? = "",
    @SerializedName("digitalId")
    var digitalId: Int? = 0,
    @SerializedName("ean")
    var ean: String? = "",
    @SerializedName("events")
    var events: Events? = Events(),
    @SerializedName("format")
    var format: String? = "",
    @SerializedName("id")
    var id: Int? = 0,
    @SerializedName("images")
    var images: List<Image>? = listOf(),
    @SerializedName("isbn")
    var isbn: String? = "",
    @SerializedName("issn")
    var issn: String? = "",
    @SerializedName("issueNumber")
    var issueNumber: Int? = 0,
    @SerializedName("modified")
    var modified: String? = "",
    @SerializedName("pageCount")
    var pageCount: Int? = 0,
    @SerializedName("resourceURI")
    var resourceURI: String? = "",
    @SerializedName("series")
    var series: Series = Series(),
    @SerializedName("stories")
    var stories: Stories = Stories(),
    @SerializedName("textObjects")
    var textObjects: List<TextObject>? = listOf(),
    @SerializedName("thumbnail")
    var thumbnail: Thumbnail? = Thumbnail(),
    @SerializedName("title")
    var title: String? = "",
    @SerializedName("upc")
    var upc: String? = "",
    @SerializedName("urls")
    var urls: List<Url>? = listOf(),
    @SerializedName("variantDescription")
    var variantDescription: String? = "",
    @SerializedName("variants")
    var variants: List<Variant>? = listOf()
)
