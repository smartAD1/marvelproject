package com.example.newapp.model.Response


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import kotlinx.serialization.Serializable

@Keep
@Serializable
data class ItemXX(
    @SerializedName("name")
    var name: String = "",
    @SerializedName("resourceURI")
    var resourceURI: String = "",
    @SerializedName("type")
    var type: String = ""
)