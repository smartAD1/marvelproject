package com.example.newapp.model.Response


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class MarvelResponse(
    @SerializedName("attributionHTML")
    var attributionHTML: String = "",
    @SerializedName("attributionText")
    var attributionText: String = "",
    @SerializedName("code")
    var code: Int = 0,
    @SerializedName("copyright")
    var copyright: String = "",
    @SerializedName("data")
    var `data`: Data = Data(),
    @SerializedName("etag")
    var etag: String = "",
    @SerializedName("status")
    var status: String = ""
)