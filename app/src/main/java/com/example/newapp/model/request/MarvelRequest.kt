package com.example.newapp.Data.response.request

data class MarvelRequest(
    var ts: Long = 0,
    var apiKey: String = "",
    var hash: String = "",
    var offset: String = "",
    var isOnRefresh: Boolean = false
)