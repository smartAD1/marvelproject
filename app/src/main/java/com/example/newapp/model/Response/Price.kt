package com.example.newapp.model.Response


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

@Keep
data class Price(
    @SerializedName("price")
    var price: Int = 0,
    @SerializedName("type")
    var type: String = ""
)