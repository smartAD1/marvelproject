package com.example.newapp.repository

import com.example.newapp.Data.response.request.MarvelRequest
import com.example.newapp.model.Response.MarvelResponse
import io.reactivex.Single
import retrofit2.Response

interface RepositoryInterFace {
    
    fun fetchRemoteData(weatherData: MarvelRequest): Single<Response<MarvelResponse>>

}