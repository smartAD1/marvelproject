package com.example.newapp.repository

import com.example.newapp.Data.response.request.MarvelRequest
import com.example.newapp.apiService.RetrofitManager
import com.example.newapp.model.Response.MarvelResponse
import com.example.newapp.model.Response.Results
import io.reactivex.Single
import retrofit2.Response

class DataRepository: RepositoryInterFace {
    private var list = arrayListOf<Results>()
    override fun fetchRemoteData(request: MarvelRequest): Single<Response<MarvelResponse>> {
        return RetrofitManager.getRetrofit().fetchData(ts = request.ts, apikey= request.apiKey,
            hash = request.hash,offset = request.offset)
            .map {
                it.body()?.let {
                    if (request.isOnRefresh){
                        list.clear()
                        list.addAll(it.data.results)
                    }else list.addAll(it.data.results)
                    it.data.results = list
                }
                it
            }
    }
}