package com.example.newapp.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.example.newapp.BuildConfig
import com.example.newapp.Data.response.request.MarvelRequest
import com.example.newapp.MarvelDataViewModel
import com.example.newapp.R
import com.example.newapp.ViewModelFactory
import com.example.newapp.adapter.MarvelAdapter
import com.example.newapp.model.Response.MarvelResponse
import com.example.newapp.statusClass.DataStatus
import com.example.newapp.utils.DateUtils
import com.example.newapp.utils.md5
import com.jcodecraeer.xrecyclerview.XRecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.util.*


class MainActivity : AppCompatActivity() {
    private val viewModel: MarvelDataViewModel by lazy {
        ViewModelProvider(this, ViewModelFactory()).get(MarvelDataViewModel::class.java)
    }
    private val adapter = MarvelAdapter()
    val dateTime = DateUtils.setTime()
    val hash = "${dateTime}${BuildConfig.PRIVATE_KEY}${BuildConfig.API_KEY}".md5()
    var nextPageSize = 0
    val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
        initEvent()
        initData()
        initVm()

    }

    private fun initView() {
        recyclerview.layoutManager = GridLayoutManager(this, 2)
        recyclerview.adapter = adapter
    }

    private fun initEvent() {
        recyclerview.setLoadingListener(object : XRecyclerView.LoadingListener {
            override fun onRefresh() {
                val marvelRequest = MarvelRequest(apiKey = BuildConfig.API_KEY,
                    ts = dateTime, hash = hash)
                viewModel.refreshData(marvelRequest)
            }

            override fun onLoadMore() {
                val marvelRequest = MarvelRequest(apiKey = BuildConfig.API_KEY, ts = dateTime,
                    hash = hash, offset = "$nextPageSize")
                viewModel.fetchData(marvelRequest)
            }
        })
        btn_onRefresh.setOnClickListener {
            it.visibility = View.GONE
            val marvelRequest = MarvelRequest(apiKey = BuildConfig.API_KEY, ts = dateTime,
                hash = hash)
            viewModel.fetchData(marvelRequest)
        }
        adapter.itemClick = {data->
                val intent = Intent(this, DescriptionActivity::class.java).apply {
                    val json = Json.encodeToString(data)
                    putExtra("results",json)
                }
                startActivity(intent)
        }
    }

    private fun initData() {
        val marvelRequest = MarvelRequest(apiKey = BuildConfig.API_KEY,
            ts = dateTime, hash = hash)
        viewModel.fetchData(marvelRequest)
    }

    private fun initVm() {
        viewModel.run {
            remoteDataLiveData.observe(this@MainActivity) {
                when (it) {
                    is DataStatus.Success<MarvelResponse> -> setData(it)
                    is DataStatus.Failure -> Log.d(TAG, "${it.message}")
                    is DataStatus.TimeOut -> Log.d(TAG, "${it.message}")
                }
            }
        }
    }

    private fun setData(it: DataStatus.Success<MarvelResponse>) {
        it.data.let {
            nextPageSize = it.data.results.size + 1
            adapter.submitList(it.data.results)
            recyclerview.visibility = View.VISIBLE
            loadDoneData()
        }
    }

    private fun loadDoneData() {
        recyclerview.loadMoreComplete()
        recyclerview.refreshComplete()
    }
    private fun showBtnOnRefresh() {
        recyclerview.visibility = View.GONE
        btn_onRefresh.visibility = View.VISIBLE
    }

}
