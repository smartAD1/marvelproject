package com.example.newapp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.newapp.R
import com.example.newapp.model.Response.Results
import kotlinx.android.synthetic.main.activity_description.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

class DescriptionActivity : AppCompatActivity() {

    var results: Results? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_description)
        intent?.let {
            results = Json.decodeFromString<Results>(it.getStringExtra("results"))
        }
        results?.let {
            Glide.with(this).load(it.thumbnail?.imgUrl)
                .into(ig_desc_icon)
            tv_desc_title.text = it.title
            tv_desc.text = it.description

        }
    }

}