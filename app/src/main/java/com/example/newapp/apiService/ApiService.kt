package com.example.newapp.apiService

import com.example.newapp.model.Response.MarvelResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("v1/public/comics")
    fun fetchData(
        @Query("ts") ts: Long,
        @Query("apikey") apikey: String,
        @Query("hash") hash: String,
        @Query("limit") limit: String = "6",
        @Query("offset") offset: String
    ): Single<Response<MarvelResponse>>
}