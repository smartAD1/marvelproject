package com.example.newapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.newapp.Data.response.request.MarvelRequest
import com.example.newapp.model.Response.MarvelResponse
import com.example.newapp.repository.RepositoryInterFace
import com.example.newapp.statusClass.DataStatus
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers


class MarvelDataViewModel(private val dataRepository: RepositoryInterFace
) : BaseViewModel() {
    private val _remoteDataStatus = MutableLiveData<DataStatus<MarvelResponse>>()
    val remoteDataLiveData: LiveData<DataStatus<MarvelResponse>>
        get() = _remoteDataStatus

    private val _loadMoreDataStatus = MutableLiveData<DataStatus<MarvelResponse>>()
    val loadMoreDataStatus: LiveData<DataStatus<MarvelResponse>>
        get() = _loadMoreDataStatus

    fun fetchData(marvelRequest: MarvelRequest) {
        dataRepository.fetchRemoteData(marvelRequest).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.isSuccessful) it?.body()?.let {
                    _remoteDataStatus.value = DataStatus.Success(it)
                }
                else _remoteDataStatus.value = it.errorBody()?.let { it -> DataStatus.Failure(it) }
            }, {
                _remoteDataStatus.value = DataStatus.TimeOut("${it.message}")
            }).addTo(compositeDisposable)
    }

    fun refreshData(marvelRequest: MarvelRequest) {
        marvelRequest.offset = "0"
        marvelRequest.isOnRefresh = true
        fetchData(marvelRequest)
    }
}