package com.example.newapp.utils

import java.sql.Timestamp
import java.util.*

object DateUtils {
    fun setTime() = Calendar.getInstance().timeInMillis.getTimeStamp()
}

fun Long.getTimeStamp() = Timestamp(this).time